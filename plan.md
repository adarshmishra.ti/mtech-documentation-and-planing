### plan

- fpga architecture
- opencl basics 
- gpu architecture 
- fpga optimizations with implementations

benchmarks 
- single item kernel
    - floyd warshll
    - inverse matrix
    - near neighbour 
    - merge sort
    - linpack 

- nd range 

    - floyd warshll
    - inverse matrix
    - near neighbour 
    - merge sort
    - linpack

- report

- andrew ng
